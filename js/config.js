$(document).ready(function(){
	if($('.carousel').length){
		$('.carousel').bxSlider({
			slideWidth: 300,
			minSlides: 1,
			maxSlides: 3,
			slideMargin: 20,
			moveSlides: 1
		});
	};

	$('#icon-message').on('click', function(){
		$('#message-list').toggle();
	});
	
	$('html').click(function(e){
		console.log(!$(e.target).closest("#messages").length);
		if(!$(e.target).closest("#messages").length && $('#message-list').is(':visible')){
			$('#message-list').hide();
		};
	});
});